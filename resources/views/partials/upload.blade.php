<div class="fileinput fileinput-new" data-provides="fileinput">
    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="line-height: 100px; width: 252px"><img
                src="{{  $old_image or ''  }}"></div>
    <div>
                                    <span class="btn btn-info btn-file waves-effect">
                                        <span class="fileinput-new">Select image</span>
                                        <input type="hidden">
                                        <input type="file" name="photo">
                                    </span>
        <a href="#" class="btn btn-danger fileinput-exists waves-effect" data-dismiss="fileinput"
           style="z-index: 900;position: relative">Remove</a>
    </div>
</div>
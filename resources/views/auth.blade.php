<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>VERIPROF</title>
    <link href="{{url('/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel panel-heading">
            LOGIN
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-3">

                </div>
                <div class="col-sm-6 col-centered"style="height: 500px">
                    <form action="{{url('auth/login')}}" method="post">
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Login">
                        </div>
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
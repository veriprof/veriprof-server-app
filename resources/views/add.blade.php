<!DOCTYPE html>
<html>
<head>
    <title>Add</title>
    <link href="{{url('/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">

    @if(Session::has('ok'))
        <div class="alert alert-success">
            <p>{{Session('ok')}}</p>
        </div>
    @endif

    <div class="panel panel-primary">
        <div class="panel-heading">New Professional</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>{{$err}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('professional/save')}}" method="post">
                        {{ csrf_field()}}
                        <label>National ID</label>
                        <input type="text" name="national_id" class="form-control" value="{{old('national_id')}}"/>

                        <label>Name</label>
                        <input type="text" name="surname" class="form-control" value="{{old('surname')}}"/>

                        <label>Other names</label>
                        <input type="text" name="other_names" class="form-control" value="{{old('other_names')}}"/>

                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="{{old('email')}}"/>

                        <label>Phone number A</label>
                        <input type="text" name="phone_a" class="form-control" value="{{old('phone_a')}}"/>

                        <label>Phone number B</label>
                        <input type="text" name="phone_b" class="form-control" value="{{old('phone_b')}}"/>

                        <label>Address</label>
                        <input type="text" name="address" class="form-control" value="{{old('address')}}"/>

                        <label>DOB</label>
                        <input type="text" name="dob" class="form-control" value="{{old('date')}}"/>

                        <br><br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <div class="col-sm-12">
                    @if(count($professionals) > 0)
                        <table class="table">
                            <thead>
                            <tr>
                                <th>National ID</th>
                                <th>Surname</th>
                                <th>Other names</th>
                                <th>Email</th>
                                <th>Phone A</th>
                                <th>Phone B</th>
                                <th>Address</th>
                                <th>DOB</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($professionals as $pro)
                                <tr>
                                    <td>{{$pro->national_id}}</td>
                                    <td>{{$pro->surname}}</td>
                                    <td>{{$pro->other_names}}</td>
                                    <td>{{$pro->email}}</td>
                                    <td>{{$pro->phone_a}}</td>
                                    <td>{{$pro->phone_b}}</td>
                                    <td>{{$pro->address}}</td>
                                    <td>{{$pro->dob}}</td>
                                    <td>
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td><a href="{{url('/edit/professional?id='. $pro->id)}}"
                                                       data-toggle="modal" data-target="#edit"
                                                       class="btn btn-primary btn-sm">&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;</a>
                                                </td>
                                                <td><a href="{{url('/delete/professional?id='. $pro->id)}}"
                                                       class="btn btn-danger btn-sm">Delete</a></td>
                                            </tr>
                                            <tr>
                                                <td><a href="{{url('/professional/credits?id='. $pro->id)}}"
                                                       class="btn btn-primary btn-sm">Credits</a></td>
                                                <td><a href="{{url('/professional/practice_hist?id='. $pro->id)}}"
                                                       class="btn btn-primary btn-sm">P. Hist&nbsp;</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {!! $professionals->render() !!}

                    @else
                        <div class="alert alert-danger">
                            <p>No professionals, please add one</p>
                        </div>
                    @endif
                </div>

                <div class="modal fade" id="edit">
                    <div class="modal-dialog">
                        <div class="modal-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{url('/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{url('/js/bootstrap.js')}}" type="text/javascript"></script>
</body>
</html>

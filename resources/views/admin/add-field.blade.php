@extends('layouts.admin-base')

@section('heading')
    ADD FRESH PROFESSIONAL
@stop

@section('content')
    <div class="col-sm-6">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('/admin/add/field')}}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label">Field name</label>
                <input type="text" name="name" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">Governing body</label>
                <select name="body">
                    @foreach($bodies as $body)
                        <option value="{{$body->id}}">{{$body->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">Description</label>
                <textarea rows="4" name="description" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@stop
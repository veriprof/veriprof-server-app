@extends('layouts.admin-base')

@section('heading')
    EDIT PROFESSIONALS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($professionals) > 0)
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Practitioner No</th>
                        <th>Field</th>
                        <th>National ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone A</th>
                        <th>Address</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($professionals as $pro)
                        <tr>
                            <td>{{$pro->practitioner_no}}</td>
                            <th>{{$pro->field()['name']}}</th>
                            <td>{{$pro->national_id}}</td>
                            <td>{{$pro->surname}} {{$pro->other_names}}</td>
                            <td>{{$pro->email}}</td>
                            <td>{{$pro->phone_a}}</td>
                            <td>{{$pro->address}}</td>
                            <td>
                                <a href="{{url('/admin/edit/details?id='. $pro->id)}}"
                                   class="btn btn-primary btn-sm">Details</a>
                                <a href="{{url('/admin/edit/credits?id='. $pro->id)}}"
                                   class="btn btn-primary btn-sm">Credits</a>
                                <a href="{{url('/admin/edit/histories?id='. $pro->id)}}"
                                   class="btn btn-primary btn-sm">Histories</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {!! $professionals->render() !!}

            @else
                <div class="alert alert-danger">
                    <p>No professionals, please add one</p>
                </div>
            @endif
        </div>
    </div>
@stop
@extends('layouts.admin-base')

@section('heading')
    EDIT DETAILS
@stop

@section('content')
    <div class="col-sm-5">
        <form action="{{url('/admin/edit/details?id='.$professional->id)}}" method="post" enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label">Practitioner No</label>
                <input type="text" name="practitioner_no" class="form-control" value="{{$professional->practitioner_no}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Field</label>
                <select name="field"  class="form-control">
                    @foreach($fields as $field)
                        <option value="{{$field->id}}" @if($field->id == $professional->field) selected="selected"@endif>{{$field->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">National ID</label>
                <input type="text" name="national_id" class="form-control" value="{{$professional->national_id}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Surname</label>
                <input type="text" name="surname" class="form-control" value="{{$professional->surname}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Other names</label>
                <input type="text" name="other_names" class="form-control" value="{{$professional->other_names}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="email" name="email" class="form-control" value="{{$professional->email}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Phone number A</label>
                <input type="text" name="phone_a" class="form-control" value="{{$professional->phone_a}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Phone number B</label>
                <input type="text" name="phone_b" class="form-control" value="{{$professional->phone_b}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Address</label>
                <input type="text" name="address" class="form-control" value="{{$professional->address}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">DOB</label>
                <input type="text" name="dob" class="form-control" value="{{$professional->dob}}"/>
            </div>
            <div class="form-group">
                @include('partials.upload', [ 'old_image' => $professional->getProfileUrl()])
            </div>
            <button type="submit" class="btn btn-primary">Save</button>

        </form>
    </div>
@stop
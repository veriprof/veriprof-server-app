@extends('layouts.admin-base')

@section('heading')
    ADD FRESH PROFESSIONAL
@stop

@section('content')
    <div class="col-sm-6">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('/admin/add/new')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label">Practitioner No</label>
                <input type="text" name="practitioner_no" class="form-control" value="{{old('practitioner_no')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Field</label>
                <select name="field" class="form-control">
                    @foreach($fields as $field)
                        <option value="{{$field->id}}">{{$field->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">National ID</label>
                <input type="text" name="national_id" class="form-control" value="{{old('national_id')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Name</label>
                <input type="text" name="surname" class="form-control" value="{{old('surname')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Other names</label>
                <input type="text" name="other_names" class="form-control" value="{{old('other_names')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="email" name="email" class="form-control" value="{{old('email')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Phone number A</label>
                <input type="text" name="phone_a" class="form-control" value="{{old('phone_a')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Phone number B</label>
                <input type="text" name="phone_b" class="form-control" value="{{old('phone_b')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Address</label>
                <input type="text" name="address" class="form-control" value="{{old('address')}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">DOB</label>
                <input type="text" name="dob" class="form-control" value="{{old('date')}}"/>
            </div>
            <div class="form-group">
                @include('partials.upload')
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@stop
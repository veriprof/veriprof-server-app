@extends('layouts.admin-base')

@section('heading')
    ADD GOVERNING BODY
@stop

@section('content')
    <div class="col-sm-6">
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('/admin/add/body')}}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
                <label class="control-label">Body name</label>
                <input type="text" name="name" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">Email</label>
                <input type="email" name="email" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">Telephone</label>
                <input type="number" name="telephone" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">Description</label>
                <textarea rows="4" name="description" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@stop
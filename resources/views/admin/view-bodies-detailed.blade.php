@extends('layouts.admin-base')

@section('heading')
    GOVERNING BODY DETAILS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(isset($body))
                <div><h4><b>DETAILS</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Telephone</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{$body->name}}</td>
                        <td>{{$body->email}}</td>
                        <td>{{$body->telephone}}</td>
                    </tr>
                    </tbody>
                </table>
                <div><h4><b>DESCRIPTION</b></h4></div>
                <div>
                    {{$body->description}}
                </div>
            @else
                <div class="alert alert-danger">
                    <p>No information for field</p>
                </div>
            @endif
        </div>
    </div>
@stop
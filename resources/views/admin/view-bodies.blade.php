@extends('layouts.admin-base')

@section('heading')
    LISTING OF GOVERNING BODIES
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($bodies) > 0)
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Telephone</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($bodies as $body)
                        <tr>
                            <td>{{$body->name}}</td>
                            <td>{{$body->email}}</td>
                            <td>{{$body->telephone}}</td>
                            <td>
                                <a href="{{url('/admin/view/bodies/detailed?id='. $body->id)}}"
                                   class="btn btn-primary btn-sm">View detailed</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {!! $bodies->render() !!}

            @else
                <div class="alert alert-danger">
                    <p>No governing bodies, please add one</p>
                </div>
            @endif
        </div>
    </div>
@stop
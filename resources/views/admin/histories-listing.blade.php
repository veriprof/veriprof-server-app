@extends('layouts.admin-base')

@section('heading')
    EDIT PROFESSIONAL DETAILS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($histories) > 0)
                <div><h4><b>PRACTICE HISTORY</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Place</th>
                        <th>Role</th>
                        <th>Address</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($histories as $history)
                        <tr>
                            <td>{{$history->place}}</td>
                            <td>{{$history->role}}</td>
                            <td>{{$history->address}}</td>
                            <td>{{$history->from}}</td>
                            <td>{{$history->to}}</td>
                            <td>
                                <a href="{{url('/admin/edit/histories/edit?id=' . $history->id)}}"
                                   class="btn btn-primary btn-sm">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">There is no practice history available for this person!</div>
            @endif
        </div>
    </div>
@stop
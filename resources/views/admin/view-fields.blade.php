@extends('layouts.admin-base')

@section('heading')
    LISTING OF PRACTICE FIELDS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($fields) > 0)
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Body</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($fields as $field)
                        <tr>
                            <td>{{$field->name}}</td>
                            <td>{{$field->body()['name']}}</td>
                            <td>
                                <a href="{{url('/admin/view/fields/detailed?id='. $field->id)}}"
                                   class="btn btn-primary btn-sm">View detailed</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {!! $fields->render() !!}

            @else
                <div class="alert alert-danger">
                    <p>No fields, please add one</p>
                </div>
            @endif
        </div>
    </div>
@stop
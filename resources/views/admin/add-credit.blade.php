@extends('layouts.admin-base')

@section('heading')
    ADD CREDIT
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <form action="{{url('/admin/add/credit')}}" method="post">
                {{ csrf_field()}}
                <input type="hidden" name="id" value="{{$id}}">
                <div class="form-group">
                    <label class="control-label">Institution</label>
                    <input type="text" name="institution" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label">Credit</label>
                    <input type="text" name="credit" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label">Year</label>
                    <input type="number" name="year" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
            </form>
        </div>
    </div>
@stop
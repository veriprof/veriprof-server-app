@extends('layouts.admin-base')

@section('heading')
    LISTING OF PROFESSIONALS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($professionals) > 0)
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Practitioner No</th>
                        <th>National ID</th>
                        <th>Surname</th>
                        <th>Other names</th>
                        <th>Email</th>
                        <th>Phone A</th>
                        <th>Practice field</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($professionals as $pro)
                        <tr>
                            <td>{{$pro->practitioner_no}}</td>
                            <td>{{$pro->national_id}}</td>
                            <td>{{$pro->surname}}</td>
                            <td>{{$pro->other_names}}</td>
                            <td>{{$pro->email}}</td>
                            <td>{{$pro->phone_a}}</td>
                            <td>{{$pro->field()['name']}}</td>
                            <td><a href="{{url('/admin/view/detailed?id='. $pro->id)}}"
                                   class="btn btn-primary btn-sm">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
                {!! $professionals->render() !!}

            @else
                <div class="alert alert-danger">
                    <p>No professionals, please add one</p>
                </div>
            @endif
        </div>
    </div>
@stop
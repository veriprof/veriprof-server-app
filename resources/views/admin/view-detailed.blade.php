@extends('layouts.admin-base')

@section('heading')
    DETAILED PROFESSIONAL INFORMATION
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div><h4><b>DETAILS</b></h4></div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Practitioner No</th>
                    <th>National ID</th>
                    <th>Surname</th>
                    <th>Other names</th>
                    <th>Email</th>
                    <th>Phone A</th>
                    <th>Phone B</th>
                    <th>Address</th>
                    <th>DOB</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$professional->practitioner_no}}</td>
                    <td>{{$professional->national_id}}</td>
                    <td>{{$professional->surname}}</td>
                    <td>{{$professional->other_names}}</td>
                    <td>{{$professional->email}}</td>
                    <td>{{$professional->phone_a}}</td>
                    <td>{{$professional->phone_b}}</td>
                    <td>{{$professional->address}}</td>
                    <td>{{$professional->dob}}</td>
                </tr>
                </tbody>
            </table>
            <div><h4><b>FIELD AND GOVERNING BODY</b></h4></div>
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Field</th>
                    <th>Governing body</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$professional->field()['name']}}</td>
                    <td>{{$professional->body()['name']}}</td>
                </tr>
                </tbody>
            </table>
            @if(count($credits) > 0)
                <div><h4><b>CREDITS</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Institution</th>
                        <th>Credit</th>
                        <th>Year</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($credits as $credit)
                        <tr>
                            <td>{{$credit->institution}}</td>
                            <td>{{$credit->credit}}</td>
                            <td>{{$credit->year}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">There are no credits available for this person!</div>
            @endif
            @if(count($histories) > 0)
                <div><h4><b>PRACTICE HISTORY</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Place</th>
                        <th>Role</th>
                        <th>Address</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($histories as $history)
                        <tr>
                            <td>{{$history->place}}</td>
                            <td>{{$history->role}}</td>
                            <td>{{$history->address}}</td>
                            <td>{{$history->from}}</td>
                            <td>{{$history->to}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">There is no practice history available for this person!</div>
            @endif
        </div>
    </div>
    @stop
@extends('layouts.admin-base')

@section('heading')
    EDIT PROFESSIONAL DETAILS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(count($credits) > 0)
                <div><h4><b>CREDITS</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Institution</th>
                        <th>Credit</th>
                        <th>Year</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($credits as $credit)
                        <tr>
                            <td>{{$credit->institution}}</td>
                            <td>{{$credit->credit}}</td>
                            <td>{{$credit->year}}</td>
                            <td>
                                <a href="{{url('/admin/edit/credits/edit?id=' . $credit->id)}}"
                                   class="btn btn-primary btn-sm">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-danger">There are no credits available for this person!</div>
            @endif
        </div>
    </div>
@stop
@extends('layouts.admin-base')

@section('heading')
    EDIT DETAILS
@stop

@section('content')
    <div class="col-sm-6">
        <form action="{{url('/admin/edit/credits/edit')}}" method="post">
            {{ csrf_field()}}
            <input type="hidden" name="id" value="{{$credit->id}}">
            <div class="form-group">
                <label class="control-label">Institution</label>
                <input type="text" name="institution" class="form-control" value="{{$credit->institution}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Credit</label>
                <input type="text" name="credit" class="form-control" value="{{$credit->credit}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Year</label>
                <input type="number" name="year" class="form-control" value="{{$credit->year}}"/>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@stop
@extends('layouts.admin-base')

@section('heading')
    ADD CREDIT
@stop

@section('content')
    <div class="row">
    <div class="col-sm-6">
        <form action="{{url('/admin/add/history')}}" method="post">
            {{ csrf_field()}}
            <input type="hidden" name="id" value="{{$id}}">
            <div class="form-group">
                <label class="control-label">Place</label>
                <input type="text" name="place" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">Role</label>
                <input type="text" name="role" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">Address</label>
                <input type="text" name="address" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="date" name="from" class="form-control">
            </div>
            <div class="form-group">
                <label class="control-label">To</label>
                <input type="date" name="to" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
    </div>
@stop
@extends('layouts.admin-base')

@section('heading')
    EDIT HISTORY ENTRY
@stop

@section('content')
    <div class="col-sm-6">
        <form action="{{url('/admin/edit/histories/edit')}}" method="post">
            {{ csrf_field()}}
            <input type="hidden" name="id" value="{{$history->id}}">
            <div class="form-group">
                <label class="control-label">Place</label>
                <input type="text" name="place" class="form-control" value="{{$history->place}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Role</label>
                <input type="text" name="role" class="form-control" value="{{$history->role}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">Address</label>
                <input type="text" name="address" class="form-control" value="{{$history->address}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">From</label>
                <input type="number" name="from" class="form-control" value="{{$history->from}}"/>
            </div>
            <div class="form-group">
                <label class="control-label">To</label>
                <input type="number" name="to" class="form-control" value="{{$history->to}}"/>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@stop
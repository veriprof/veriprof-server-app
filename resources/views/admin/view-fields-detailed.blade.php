@extends('layouts.admin-base')

@section('heading')
    PRACTICE FIELD DETAILS
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if(isset($field))
                <div><h4><b>DETAILS</b></h4></div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Body</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$field->name}}</td>
                            <td>{{$field->body()['name']}}</td>
                        </tr>
                    </tbody>
                </table>
                <div><h4><b>DESCRIPTION</b></h4></div>
                <div>
                    {{$field->description}}
                </div>
            @else
                <div class="alert alert-danger">
                    <p>No information for field</p>
                </div>
            @endif
        </div>
    </div>
@stop
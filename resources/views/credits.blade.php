<!DOCTYPE html>
<html>
<head>
    <title>Add</title>
    <link href="{{url('/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/css/style.css')}}" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">

    @if(Session::has('ok'))
        <div class="alert alert-success">
            <p>{{Session('ok')}}</p>
        </div>
    @endif

    <div class="panel panel-primary">
        <div class="panel-heading">New Credit</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $err)
                                    <li>{{$err}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{url('credit/save')}}" method="post">
                        {{ csrf_field()}}
                        <label>Institution</label>
                        <input type="text" name="institution" class="form-control" value="{{old('institution')}}"/>

                        <label>Credit</label>
                        <input type="text" name="credit" class="form-control" value="{{old('credit')}}"/>

                        <label>Year</label>
                        <input type="text" name="year" class="form-control" value="{{old('year')}}"/>
                        <br><br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <div class="col-sm-12">
                    @if(count($credits) > 0)
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Institution</th>
                                <th>Credit</th>
                                <th>Year names</th>
                                <th colspan="4">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($credits as $cred)
                                <tr>
                                    <td>{{$cred->institution}}</td>
                                    <td>{{$cred->credit}}</td>
                                    <td>{{$cred->year}}</td>
                                    <td>
                                        <table class="table">

                                            <tbody>
                                            <tr>
                                                <td><a href="{{url('/edit/credit?id='. $cred->id)}}" data-toggle="modal" data-target="#edit" class="btn btn-primary btn-sm">&nbsp;&nbsp;&nbsp;Edit&nbsp;&nbsp;&nbsp;</a></td>
                                                <td><a href="{{url('/delete/credit?id='. $cred->id)}}" class="btn btn-danger btn-sm">Delete</a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {!! $credits->render() !!}

                    @else
                        <div class="alert alert-danger">
                            <p>No professionals, please add one</p>
                        </div>
                    @endif
                </div>

                <div class="modal fade" id="edit">
                    <div class="modal-dialog">
                        <div class="modal-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{url('/js/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{url('/js/bootstrap.js')}}" type="text/javascript"></script>
</body>
</html>

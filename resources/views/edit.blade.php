<div class="row">
    <div class="col-sm-12">
        <form action="{{url('/professional/edit?id='.$prop->id)}}" method="post">
            {{ csrf_field()}}
            <label>National ID</label>
            <input type="text" name="national_id" class="form-control" value="{{$prop->national_id}}"/>

            <label>Surname</label>
            <input type="text" name="surname" class="form-control" value="{{$prop->surname}}"/>

            <label>Other names</label>
            <input type="text" name="other_names" class="form-control" value="{{$prop->other_names}}"/>

            <label>Email</label>
            <input type="email" name="email" class="form-control" value="{{$prop->email}}"/>

            <label>Phone number A</label>
            <input type="text" name="phone_a" class="form-control" value="{{$prop->phone_a}}"/>

            <label>Phone number B</label>
            <input type="text" name="phone_b" class="form-control" value="{{$prop->phone_b}}"/>

            <label>Address</label>
            <input type="text" name="address" class="form-control" value="{{$prop->address}}"/>

            <label>DOB</label>
            <input type="text" name="dob" class="form-control" value="{{$prop->dob}}"/>

            <br><br>
            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>


</div>
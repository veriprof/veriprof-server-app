<?php header("Content-type: application/json");?>

{
	"error":
	{
		"code" : 404,
		"message" : "The resource requested could not be located. Please read the API documentation"
	}
}


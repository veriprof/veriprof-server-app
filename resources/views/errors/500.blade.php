<?php header("Content-type: application/json");?>

{
    "error":
    {
        "code" : 500,
        "message" : "The server does not have the capacity to handle your request"
    }
}


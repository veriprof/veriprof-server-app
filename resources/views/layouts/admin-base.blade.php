<!DOCTYPE html>
<html>
<head>
    <title>Veriprof</title>
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{url('css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('css/style.css')}}" rel="stylesheet" type="text/css">

    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.js')}}"></script>
    <script src="{{ url('js/plugins/fileinput/fileinput.min.js')}}"></script>
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/admin/home')}}">VERIPROF</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="{{url('admin/view')}}">Professionals</a>
                </li>
                <li class="">
                    <a href="{{url('admin/edit')}}">Edit professionals</a>
                </li>
                <li class="">
                    <a href="{{url('admin/add-new')}}">+ new professionals</a>
                </li>
                <li class="">
                    <a href="{{url('admin/add')}}">+ credits and histories</a>
                </li>
                <li class="">
                    <a href="{{url('admin/view/bodies')}}">Governing bodies</a>
                </li>
                <li class="">
                    <a href="{{url('admin/view/fields')}}">Practice fields</a>
                </li>
                <li class="">
                    <a href="{{url('/admin/add/body')}}">+ governing bodies</a>
                </li>
                <li class="">
                    <a href="{{url('/admin/add/field')}}">+ practice fields</a>
                </li>
                <li class="">
                    <a href="{{url('auth/logout')}}">Logout</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container" style="padding:0;width:90%;">
        @if(Session::has('ok'))
            <div class="alert alert-success">
                <p>{{Session('ok')}}</p>
            </div>
        @endif
            <div class="panel-body">
                @yield('content')
            </div>
    </div>
</body>
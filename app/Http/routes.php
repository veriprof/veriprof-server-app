<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//GENERAL
//api
Route::get('/api/details', 'GeneralController@api_details');

Route::get('/api/all', 'GeneralController@api_all');

Route::group(['prefix' => 'admin', 'middleware' => 'auth.basic'], function() {
    Route::get('/home', 'GeneralController@home');

    Route::get('/add-new', 'GeneralController@view_add_new');

    Route::get('/edit', 'GeneralController@view_edit');//G

    Route::get('/add', 'GeneralController@view_add');//G

    Route::get('/view', 'GeneralController@view');//G

    Route::get('/view/detailed', 'GeneralController@view_detailed');//G

//EDIT
//details
    Route::get('/edit/details', 'DetailController@show_current');

    Route::post('/edit/details', 'DetailController@save');

//credits
    Route::get('/edit/credits', 'CreditController@credits_listing');

    Route::get('/edit/credits/edit', 'CreditController@show_current');

    Route::post('/edit/credits/edit', 'CreditController@save');

//histories
    Route::get('/edit/histories', 'HistoryController@histories_listing');

    Route::get('/edit/histories/edit', 'HistoryController@show_current');

    Route::post('/edit/histories/edit', 'HistoryController@save');

//ADD
//new
    Route::post('/add/new', 'DetailController@add_new');

//credit
    Route::get('/add/credit', 'CreditController@show_add');

    Route::post('/add/credit', 'CreditController@add');

//histories
    Route::get('/add/history', 'HistoryController@show_add');

    Route::post('/add/history', 'HistoryController@add');

//body
    Route::get('/add/body', 'BodyController@index');

    Route::post('/add/body', 'BodyController@add');

//field
    Route::get('/add/field', 'FieldController@index');

    Route::post('/add/field', 'FieldController@add');


//VIEW
//bodies
    Route::get('/view/bodies', 'BodyController@show_all');

    Route::get('/view/bodies/detailed', 'BodyController@show_detailed');

//fields
    Route::get('/view/fields', 'FieldController@show_all');

    Route::get('/view/fields/detailed', 'FieldController@show_detailed');

//VERIPROF
    Route::post('/veriprof', 'DetailController@add_new');
});

Route::group(['prefix' => 'auth'], function(){
    Route::get('/', 'Auth\AuthController@showLogin');

    Route::get('/login', 'Auth\AuthController@showLogin');

    Route::post('/login', 'Auth\AuthController@attempt');

    Route::any('/logout', 'Auth\AuthController@logout');
});


Route::get('index', function(){
    return view('admin.index');
});

//Images / blob
Route::pattern('filename', '[a-z0-9.]+');
Route::any('/images/{filename}', 'ImageController@getImage');
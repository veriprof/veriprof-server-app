<?php


namespace App\Http\Controllers;


use App\Image;
use Response;
use Storage;

class ImageController extends Controller
{
    public function getImage($filename)
    {
        $path = Image::getStoragePath() . '/' . $filename;
        //dd($path);
        $blob = @file_get_contents($path);
        if (!$blob) {
            $blob = '';
        }
        return response($blob, 200, ['Content-Type' => 'image/jpg']);
    }
}
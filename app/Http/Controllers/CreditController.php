<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Detail;

class CreditController extends Controller
{
    //add credit details of a professional
    public function add()
    {
        $input = Input::all();
        $rules = [
            'id' => 'required|numeric',
            'institution' => 'required',
            'credit' => 'required',
            'year' => 'numeric|required'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            Credit::create(
                [
                    'owner_id' => $input['id'],
                    'institution' => $input['institution'],
                    'credit' => $input['credit'],
                    'year' => $input['year']
                ]
            );
            $msg = "Credit added successfully";
            return redirect()->back()->with('ok', $msg);
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    //provide a listing of all credits of professional to allow selection of which to edit
    public function credits_listing()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $professional = Detail::find($id);
        if(isset($professional))
        {
            $credits = $professional->credits();
            return view('admin.credits-listing', ['credits' => $credits]);
        }
        else
            abort(404);
    }

    public function show_current()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $credit = Credit::find($id);
        if(isset($credit))
        {
            return view('admin.edit-credit', ['credit' => $credit]);
        }
        else
            abort(404);
    }

    //allow updating after editing
    public function save()
    {
        $input = Input::all();
        $rules = [
            'id' => 'required|numeric',
            'institution' => 'required',
            'credit' => 'required',
            'year' => 'numeric|required'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            $credit = Credit::find($input['id']);
            if(isset($credit))
            {
                $credit->institution = $input['institution'];
                $credit->credit = $input['credit'];
                $credit->year = $input['year'];
                $credit->save();
                $msg = "Credit edited successfully";
                return redirect()->back()->with('ok', $msg);
            }
            else
            {
                abort(404);
            }
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    public function show_add()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        return view('admin.add-credit', ["id" => $id]);
    }
}

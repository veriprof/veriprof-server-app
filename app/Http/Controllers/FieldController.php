<?php

namespace App\Http\Controllers;

use App\Body;
use App\Http\Requests;
use App\Field;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class FieldController extends Controller
{
    public function index()
    {
        $bodies = Body::all();
        return view('admin.add-field', ["bodies" => $bodies]);
    }

    public function add()
    {
        $input = Input::all();
        $rules = [
            'name' => 'required|string',
            'body' => 'required|numeric',
            'description' => 'required|string'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            Field::create(
                [
                    'name' => $input['name'],
                    'body_id' => $input['body'],
                    'description' => $input['description']
                ]
            );
            $msg = "Practice field added successfully";
            return redirect()->back()->with('ok', $msg);
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    public function show_all()
    {
        $fields = Field::paginate(11);
        return view('admin.view-fields', ['fields' => $fields]);
    }

    public function show_detailed()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $field = Field::find($id);
        return view('admin.view-fields-detailed', ['field' => $field]);
    }
}

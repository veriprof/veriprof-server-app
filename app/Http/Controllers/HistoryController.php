<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\History;
use Illuminate\Support\Facades\Input;
use App\Detail;
use Illuminate\Support\Facades\Validator;

class HistoryController extends Controller
{
    //add history details of a professional
    public function add()
    {
        $input = Input::all();
        $rules = [
            'id' => 'required|numeric',
            'place' => 'required',
            'role' => 'required',
            'address' => 'required',
            'from' => 'date|required',
            'to' => 'date|required',
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            History::create(
                [
                    'owner_id' => $input['id'],
                    'place' => $input['place'],
                    'role' => $input['role'],
                    'address' => $input['address'],
                    'from' => $input['from'],
                    'to' => $input['to']
                ]
            );
            $msg = "History added successfully";
            return redirect()->back()->with('ok', $msg);
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    public function histories_listing()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $professional = Detail::find($id);
        if(isset($professional))
        {
            $histories = $professional->histories();
            return view('admin.histories-listing', ['histories' => $histories]);
        }
        else
            abort(404);
    }

    public function show_current()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $history = History::find($id);
        if(isset($history))
        {
            return view('admin.edit-history', ['history' => $history]);
        }
        else
            abort(404);
    }

    public function save()
    {
        $input = Input::all();
        $rules = [
            'id' => 'required|numeric',
            'place' => 'required',
            'role' => 'required',
            'address' => 'required',
            'from' => 'required',
            'to' => 'required'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            $history = History::find($input['id']);
            if(isset($history))
            {
                $history->place = $input['place'];
                $history->role = $input['role'];
                $history->address = $input['address'];
                $history->from = $input['from'];
                $history->to = $input['to'];
                $history->save();
                $msg = "History edited successfully";
                return redirect()->back()->with('ok', $msg);
            }
            else
            {
                abort(404);
            }
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    public function show_add()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        return view('admin.add-history', ["id" => $id]);
    }
}

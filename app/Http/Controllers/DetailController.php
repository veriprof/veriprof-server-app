<?php

namespace App\Http\Controllers;

use App\Detail;
use App\Field;
use App\Http\Requests;
use App\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DetailController extends Controller
{
    //provide current details to user to be edited
    public function show_current()
    {
        $id = (int)Input::get('id');
        if (!is_int($id)) {
            abort(404);
        }
        $professional = Detail::find($id);
        $fields = Field::all(['id', 'name']);
        if (isset($professional) && isset($fields)) {
            return view('admin.edit-details', ['professional' => $professional, 'fields' => $fields]);
        } else
            abort(404);
    }

    //save the modifications made to the details
    public function save()
    {
        $input = Input::all();
        $rules = [
            'id' => 'required|numeric',
            'surname' => 'required',
            'other_names' => 'required',
            'practitioner_no' => 'required',
            'national_id' => 'required|numeric',
            'email' => 'email|required',
            'phone_a' => 'required|numeric',
            'phone_b' => 'required|numeric',
            'address' => 'required',
            'dob' => 'required|date',
            'field' => 'required|numeric'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            $professional = Detail::find($input['id']);
            $this->updatePhoto(Input::file('photo'), $professional);
            $professional->practitioner_no = $input['practitioner_no'];
            $professional->surname = $input['surname'];
            $professional->other_names = $input['other_names'];
            $professional->national_id = $input['national_id'];
            $professional->email = $input['email'];
            $professional->phone_a = $input['phone_a'];
            $professional->phone_b = $input['phone_b'];
            $professional->address = $input['address'];
            $professional->dob = $input['dob'];
            $professional->field = $input['field'];
            $professional->save();
            $msg = "Details updated successfully";
            return redirect()->back()->with('ok', $msg);
        } else {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    //add a new professional by adding their details to the database
    public function add_new()
    {
        $input = Input::all();
        $rules = [
            'surname' => 'required',
            'other_names' => 'required',
            'practitioner_no' => 'required',
            'national_id' => 'required|numeric',
            'email' => 'email|required',
            'phone_a' => 'required|numeric',
            'phone_b' => 'required|numeric',
            'address' => 'required',
            'dob' => 'required|date',
            'field' => 'required|numeric'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes()) {
            $professional = new Detail(
                [
                    'practitioner_no' => $input['practitioner_no'],
                    'surname' => $input['surname'],
                    'other_names'=> $input['other_names'],
                    'national_id' => $input['national_id'],
                    'email' => $input['email'],
                    'phone_a' => $input['phone_a'],
                    'phone_b' => $input['phone_b'],
                    'address' => $input['address'],
                    'dob' => $input['dob'],
                    'field' => $input['field']
                ]
            );
            $professional->save();
            $this->updatePhoto(Input::file('photo'), $professional);
            $msg = "Professional added successfully";
            return redirect()->back()->with('ok', $msg);
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    private function updatePhoto($photo, Detail $detail)
    {
        if ($photo) {
            $name = md5('member_passport' . $detail->id) . '.' . $photo->guessExtension();
            $photo->move(Image::getStoragePath(), $name);
            $photo = $detail->getPhoto();
            $photo->owner_id = $detail->id;
            $photo->setImage(Image::create(['filename' => $name]));
            $photo->save();
        }

    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Body;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class BodyController extends Controller
{
    public function index()
    {
        return view('admin.add-body');
    }

    public function add()
    {
        $input = Input::all();
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'telephone' => 'numeric|required',
            'description' => 'required|string'
        ];
        $val = Validator::make($input, $rules);
        if ($val->passes())
        {
            Body::create(
                [
                    'name' => $input['name'],
                    'email' => $input['email'],
                    'telephone' => $input['telephone'],
                    'description' => $input['description']
                ]
            );
            $msg = "Governing body added successfully";
            return redirect()->back()->with('ok', $msg);
        }
        else
        {
            return redirect()->back()->withInput($input)->withErrors($val->errors());
        }
    }

    public function show_all()
    {
        $bodies = Body::paginate(11);
        return view('admin.view-bodies', ['bodies' => $bodies]);
    }

    public function show_detailed()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $body = Body::find($id);
        return view('admin.view-bodies-detailed', ['body' => $body]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Detail;
use Illuminate\Support\Facades\Input;
use App\Credit;
use App\History;
use Illuminate\Support\Facades\Response;
use App\Field;

class GeneralController extends Controller
{
    public function home()
    {
        return view('admin.home');
    }

    //show a listing of all the professionals displaying only the basic information
    public function view()
    {
        $professionals = Detail::paginate(11);
        return view('admin.view', ['professionals' => $professionals]);
    }

    //display a listing of all professionals providing options to add credits and histories
    public function view_add()
    {
        $professionals = Detail::paginate(11);
        return view('admin.add', ['professionals' => $professionals]);
    }

    public function view_add_new()
    {
        $fields = Field::all(['id', 'name']);
        if(isset($fields))
        {
            return view('admin.add-new', ['fields' => $fields]);
        }
    }

    public function view_detailed()
    {
        $id =  (int)Input::get('id');
        if(!is_int($id))
        {
            abort(404);
        }
        $professional = Detail::find($id);
        if(isset($professional))
        {
            $credits = Credit::where('owner_id', $id)->get();
            $histories = History::where('owner_id', $id)->get();
            return view('admin.view-detailed',
                [
                    'professional' => $professional,
                    'credits' => $credits,
                    'histories' => $histories
                ]);
        }
        else
            abort(404);
    }

    //display a listing of all professionals providing options to edit details, credits and histories
    public function view_edit()
    {
        $professionals = Detail::paginate(11);
        return view('admin.edit', ['professionals' => $professionals]);
    }

    public function api_all()
    {
        $practitioner = (string)Input::get('pract');
        if (!is_string($practitioner)) {
            abort(404);
        }
        $details = Detail::where('practitioner_no',  '=', $practitioner)->first();
        $details->photo =  $details->getProfileUrl();
        $id = $details['id'];
        if ($details) {
            $details->credits = Credit::where('owner_id', $id)->get();
            $details->practice = History::where('owner_id', $id)->get();
            return Response::json($details, 200, [], JSON_PRETTY_PRINT);
        } else {
            abort(404);
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Image extends Model
{
    protected $guarded = ['id'];

    public static function getStoragePath()
    {
        return storage_path('images');
    }

    public function getUrl()
    {
        if ($this->filename == null) {
            return url('images/avatar-small.png');
        } else {
            return url('images/' . $this->filename);
        }
    }

    public function preProcess()
    {

    }

    public function cleanUp()
    {
        try {
            Storage::delete(self::getStoragePath() . $this->filename);
        } catch (\Exception $e) {

        }
        $this->delete();
    }

}

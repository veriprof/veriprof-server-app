<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = "histories";
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = 'credits';
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $table = "details";
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function credits()
    {
        return Credit::where('owner_id', '=', $this->id)->get();
    }

    public function histories()
    {
        return History::where('owner_id', '=', $this->id)->get();
    }

    public function field()
    {
        return Field::find($this->field);
    }

    public function getPhoto()
    {
        $passport = Profile::where('owner_id', $this->id)->first();
        if (!$passport) {
            $passport = new Profile();
        }
        return $passport;
    }

    public function getProfileUrl()
    {
        return $this->getPhoto()->getImage()->getUrl();
    }
    
    public function body()
    {
        $body = Field::find($this->field)['body_id'];
        return Body::find($body);
    }
}
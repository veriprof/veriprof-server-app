<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $table = "bodies";
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];
}

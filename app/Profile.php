<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function getImage()
    {
        $image = Image::find($this->image_id);
        if (!$image) {
            $image = new Image();
        }

        return $image;
    }

    public function setImage($image)
    {
        $this->getImage()->cleanUp();
        $this->image_id = $image->id;
    }

    public function cleanUp()
    {
        $this->getImage()->cleanUp();
        $this->delete();
    }
}

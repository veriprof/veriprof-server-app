<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = "fields";
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function body()
    {
        return Body::find($this->body_id);
    }
}

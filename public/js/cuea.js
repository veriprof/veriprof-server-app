/**
 * Created by griffins on 10/22/15.
 */

$(document).ready(function (e) {
    $('#f_select').on('change', function () {
        loadCourses(this.value);
    });
    loadCourses($('#f_select').val());
});

function loadCourses(v) {
    $.get('api/courses?f=' + v, function (courses, status) {
        var html = "";
        for (x = 0; x < courses.length; x++) {
            var option = "<option value = " + courses[x].id + " > " + courses[x].name + "</option>";
            html = html + option;
        }
        $('#c_select').html(html);
    });
}
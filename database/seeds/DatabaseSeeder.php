<?php

use App\Credit;
use App\History;
use App\Detail;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(CreditsTableSeeder::class);
        $this->call(PracticeTableSeeder::class);

        Model::reguard();
    }
}

class MasterTableSeeder extends Seeder
{

    public function run()
    {
        Detail::create(
            [
                "practitioner_no" => "F7R4BF4NIVD",
                "national_id" => '31870918',
                "surname" => 'Mwiti',
                "other_names" => 'Mutai M',
                "dob" => '1994-10-28',
                "address" => 'P.O. BOX 11 Kionyo',
                "email" => 'mutaimwiti40@gmail.com',
                "phone_a" => '0710902149',
                "phone_b" => '0713455456'
            ]
        );
        Detail::create(
            [
                "practitioner_no" => "HGD74356458",
                "national_id" => '34324896',
                "surname" => 'Oringo',
                "other_names" => 'Griffins',
                "dob" => '1994-05-20',
                "address" => 'P.O. BOX 66 Kisii',
                "email" => 'oringogriffins@gmail.com',
                "phone_a" => '0743433451',
                "phone_b" => '0706555434'
            ]
        );
    }
}

class CreditsTableSeeder extends Seeder
{

    public function run()
    {
        //first person
        Credit::create(
            [
                "owner_id" => '1',
                "institution" => 'Multimedia University',
                "credit" => 'Bsc Degree in Information Technology',
                "year" => '2012',
            ]
        );
        Credit::create(
            [
                "owner_id" => '1',
                "institution" => 'Norway University of Technology',
                "credit" => 'Masters Degree in Software Engineering',
                "year" => '2015',
            ]
        );
        //second person
        Credit::create(
            [
                "owner_id" => '2',
                "institution" => 'JKUAT University',
                "credit" => 'Bsc Degree in Computer Science',
                "year" => '2011',
            ]
        );
        Credit::create(
            [
                "owner_id" => '2',
                "institution" => 'MIT',
                "credit" => 'Masters Degree in Informatics',
                "year" => '2012',
            ]
        );
    }
}

class PracticeTableSeeder extends Seeder
{

    public function run()
    {
        //first person
        History::create(
            [
                "owner_id" => '1',
                "place" => 'Multimedia University',
                "role" => 'Assistant Lecturer',
                "address" => 'P.O Box 1 Mbagathi',
                "from" => '2012',
                "to" => '2013',
            ]
        );
        History::create(
            [
                "owner_id" => '1',
                "place" => 'Oracle Kenya',
                "role" => 'Lead Database Engineer',
                "address" => 'P.O Box 220 Nairobi',
                "from" => '2015',
                "to" => null,
            ]
        );
        //second person
        History::create(
            [
                "owner_id" => '2',
                "place" => 'Spartan Africa',
                "role" => 'Senior web applications developer',
                "address" => 'P.O Box 23 Nairobi',
                "from" => '2013',
                "to" => '2014',
            ]
        );
        History::create(
            [
                "owner_id" => '2',
                "place" => 'Core Tec',
                "role" => 'Lead Web Engineer',
                "address" => 'P.O Box 110 Nairobi',
                "from" => '2015',
                "to" => null,
            ]
        );
    }
}

class UserTableSeeder extends Seeder
{

    public function run()
    {
        User::create(
            [
                "username" => "mutaimwiti",
                "first_name" => "Mutai",
                "last_name" => "Mwiti",
                "email" => "mutaimwiti40@gmail.com",
                "password" => Hash::make("passxxx"),
                "role" => 1
            ]
        );
        User::create(
            [
                "username" => "mwitimburugu",
                "first_name" => "Mwiti",
                "last_name" => "Mburugu",
                "email" => "mwitimburugu@yahoo.com",
                "password" => Hash::make("pass101"),
                "role" => 1
            ]
        );
    }
}
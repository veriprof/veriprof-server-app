<?php

use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function ($table) {
            $table->increments('id');
            $table->string('filename');
            $table->timestamps();
        });
        Schema::create('profiles', function ($table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('image_id', false);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Schema::dropIfExists('profiles');
    }
}

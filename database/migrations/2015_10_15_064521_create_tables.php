<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details', function (Blueprint $table){
	        $table->increments('id');
            $table->string('practitioner_no')->unique();
	        $table->string('national_id',8)->unique();
	        $table->string('surname');
	        $table->string('other_names');
	        $table->date('dob');
	        $table->string('address');
	        $table->string('email')->unique();
	        $table->string('phone_a');
	        $table->string('phone_b');
            $table->integer('field')->notNull();
            $table->timestamps();
	    });
        //$table->foreign('user_id')->references('id')->on('users');
        Schema::create('credits', function (Blueprint $table){
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->string('institution');
            $table->string('credit');
            $table->integer('year', false);
            $table->timestamps();
            $table->foreign('owner_id')->references('id')->on('details');
        });

        Schema::create('histories', function (Blueprint $table){
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->string('place');
            $table->string('role');
            $table->string('address');
            $table->integer('from', false);
            $table->integer('to', false)->nullable();
            $table->timestamps();
            $table->foreign('owner_id')->references('id')->on('details');
        });

        Schema::create('bodies', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('telephone');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('fields', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('body_id')->unsigned();
            $table->string('description', 1024);
            $table->timestamps();
            $table->foreign('body_id')->references('id')->on('bodies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details');
        Schema::dropIfExists('credits');
        Schema::dropIfExists('histories');
        Schema::dropIfExists('fields');
        Schema::dropIfExists('bodies');
    }
}
